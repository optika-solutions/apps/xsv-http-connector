"""
HTTP xSV Connector
"""
import requests
from detect_delimiter import detect
import shutil
import csv
import os


def download_file(url):
    local_filename = url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        shutil.copyfileobj(r.raw, f)

    return local_filename


def akumen(**kwargs):
    """
    !! This akumen() function must exist in the execution file!

    Parameters:
        !! These lines define parameters, and a line must exist per input (or output).

        - Input: data_url [string]
        - Input: delimiter [string]
        - Input: skiprows [int]
        - Input: skipcols [int]

        - Output: data [file] (data.csv)
    """
    print('Running Akumen model...')
    
    data_url = kwargs.get('data_url')
    
    if data_url is None:
        raise Exception('`data_url` must be specified as an input.')
    
    filename = download_file(data_url)
    
    skiprows = kwargs.get('skiprows', 0)
    skipcols = kwargs.get('skipcols', 0)
    
    with open(filename) as file:
        if not kwargs.get('delimiter'):
            # get the header row
            for i, line in enumerate(file):
                if i == skiprows:
                    header = line
                    break
            
            delimiter = detect(line)
            if delimiter is None:
                raise Exception('Was unable to detect a delimiter for the data: specify your own, or make sure that skiprows is set to the header row.')
            file.seek(0)
        else:
            delimiter = kwargs.get('delimiter')
        
        with open('outputs/data.csv', 'w') as file_out:
            reader = csv.DictReader(file, delimiter=delimiter)
            writer = csv.DictWriter(file_out, reader.fieldnames, delimiter=',')
            writer.writeheader()
            writer.writerows(reader)
    
    return {}
